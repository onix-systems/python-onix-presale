# Django CRUD TODO

## Install vagrant

###Ubuntu  
    sudo apt-get update
    sudo apt-get install -y virtualbox
    sudo apt-get install -y vagrant
    sudo apt-get install -y virtualbox-dkms

### OS X
    brew cask install virtualbox
    brew cask install vagrant
    brew cask install vagrant-manager
## Run project
### You should stop apache or nginx because vagrant bind to 80 port
    vagrant up 
    
## Open site on browser 

    http://192.168.33.10
    