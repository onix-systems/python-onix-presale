"""untitled1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from taskmanager import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/login/$', views.Login.as_view(), name='login'),
    url(r'^accounts/signup/$', views.signup, name='signup'),
    url(r'^$', views.ItemListView.as_view(), name='items'),
    url(r'^create/$', views.ItemCreateView.as_view(), name='create'),
    url(r'^detail/(?P<pk>\d+)$', views.ItemDetailView.as_view(), name='detail'),
    url(r'^edit/(?P<pk>\d+)$', views.ItemUpdateView.as_view(), name='edit'),
    url(r'^delete/(?P<pk>\d+)$', views.ItemDeleteView.as_view(), name='delete'),
    url(r'^mark_done/(?P<pk>\d+)$', views.UpdateDone.as_view(), name='mark_done'),
]
