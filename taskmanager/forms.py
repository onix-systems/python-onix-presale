from django import forms
from taskmanager.models import Item


class ItemForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = ['name', 'description', 'status', 'user']
