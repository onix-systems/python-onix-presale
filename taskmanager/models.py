# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Item(models.Model):
    TODO = 1
    DONE = 2
    UNDONE = 3
    DELETE = 4

    user = models.ForeignKey('auth.User', related_name='user')
    name = models.CharField(max_length=256, help_text='Task name')
    status = models.IntegerField(default=TODO, help_text='Task status default value 1')
    result = models.ForeignKey('auth.User', null=True, related_name='result')
    description = models.TextField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
