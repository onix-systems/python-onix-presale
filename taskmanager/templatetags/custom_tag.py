from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()


@register.filter
def index(value, args):
    if value and len(value)-1 >= args:
        return value[args]
    else:
        return []
