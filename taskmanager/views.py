# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import views
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from taskmanager.models import Item
from django.views.generic import ListView, DetailView, CreateView, DeleteView, UpdateView
from taskmanager.forms import ItemForm
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse_lazy
from django.db.models import Q
from django.forms.utils import ErrorList
from django import forms


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('items')

    else:
        form = UserCreationForm()

    return render(request, 'registration/signup.html', {'form': form})


class Login(views.LoginView):
    success_url = 'items'


class Mixin(object):
    model = Item
    login_url = 'login'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        """
        Protect method for authorization
        """
        return super(Mixin, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return self.model.objects.filter(pk=self.kwargs['pk'])


class ItemListView(Mixin, ListView):
    template_name = 'taskmanager/list.html'
    paginate_by = 10

    def get_queryset(self, **kwargs):
        value = self.request.GET.get('status', None)

        status = {'status': value} if value and int(value) != 0 else {}

        return self.model.objects.filter(~Q(status=self.model.DELETE), **status).select_related('user', 'result')\
            .order_by(self.request.GET.get("order", 'pk'))


class ItemDetailView(Mixin, DetailView):
    pass


class ItemCreateView(Mixin, CreateView):
    success_url = reverse_lazy('items')
    template_name = 'taskmanager/item_create.html'
    form_class = ItemForm


class ItemDeleteView(Mixin, DeleteView):
    success_url = reverse_lazy('items')

    fields = []
    error_template = 'taskmanager/error.html'

    def form_valid(self, form):
        record = self.model.objects.get(**self.kwargs)

        if record.user.id != self.request.user.id:
            form._errors[forms.forms.NON_FIELD_ERRORS] = ErrorList([
                u"You can't delete a foreign record"
            ])
            return self.render_to_response(self.get_context_data(form=form), status=400)

        record.status = self.model.DELETE
        record.save()
        return redirect(self.get_success_url())


class ItemUpdateView(Mixin, UpdateView):
    success_url = reverse_lazy('items')
    form_class = ItemForm

    def form_valid(self, form):
        data = form.cleaned_data
        record = self.model.objects.get(**self.kwargs)
        if record.user.id != self.request.user.id:
            form._errors[forms.forms.NON_FIELD_ERRORS] = ErrorList([
                u"You can't update a foreign record"
            ])
            return self.form_invalid(form)

        if data['status'] in (self.model.DONE, self.model.UNDONE):
            data['result'] = self.request.user

        for k, v in data.items():
            if hasattr(record, k):
                setattr(record, k, v)

        record.save()
        return redirect(self.get_success_url())


class UpdateDone(Mixin, UpdateView):
    success_url = reverse_lazy('items')

    fields = []
    error_template = 'taskmanager/error.html'

    def get_queryset(self):
        return self.model.objects.filter(pk=self.kwargs['pk'])

    def form_valid(self, form):
        record = self.model.objects.get(**self.kwargs)

        if record.status != self.model.TODO:
            form._errors[forms.forms.NON_FIELD_ERRORS] = ErrorList([
                u'Record has finished'
            ])
            return self.render_to_response(self.get_context_data(form=form), status=400)

        record.status = self.model.DONE
        record.result = self.request.user
        record.save()
        return redirect(self.get_success_url())


